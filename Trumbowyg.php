<?php

namespace azbuco\trumbowyg;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class Trumbowyg extends InputWidget {

    public $options = [
        'class' => 'form-control',
    ];
    public $clientOptions = [
    ];
    public $plugins = [
    ];
    public $language;

    public function init()
    {
        parent::init();

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        
        if (!$this->language) {
            $this->language = strtolower(substr(\Yii::$app->language, 0, 2));
        }
    }

    public function run()
    {
        $this->registerClientScript();

        return $this->hasModel() ?
        Html::activeTextarea($this->model, $this->attribute, $this->options) :
        Html::textarea($this->name, $this->value, $this->options);
    }

    protected function registerClientScript()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        
        $asset = TrumbowygAsset::register($view);
        $asset->language = $this->language;
        $asset->plugins = $this->plugins;
        
        $this->clientOptions['lang'] = $this->language;
        
        $config = Json::encode($this->clientOptions);

        $js = ";$('#{$id}').trumbowyg({$config});\n";
        $view->registerJs($js);
    }

}
