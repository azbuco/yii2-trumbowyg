<?php

namespace azbuco\trumbowyg;

use yii\web\AssetBundle;

class TrumbowygAsset extends AssetBundle {

    public $sourcePath = '@bower/trumbowyg/dist';
    public $css = [
        'ui/trumbowyg.css',
    ];
    public $js = [
        'trumbowyg.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * @var string language
     */
    public $language;

    /**
     * @var array plugins array
     */
    public $plugins = [];

    public function registerAssetFiles($view)
    {
        if ($this->language) {
            $this->js[] = 'langs/' . $this->language . '.min.js';
        }

        foreach ($this->plugins as $plugin) {
            $this->js[] = 'plugins/' . $plugin . '/trumbowyg.' . $plugin . '.min.js';
        }

        parent::registerAssetFiles($view);
    }

}
