# Yii2 widget fro Trumbowyg #

## Usage example ##

```
<?=
azbuco\trumbowyg\Trumbowyg::widget([
    'clientOptions' => [
        'btnsDef' => [
            'formattingLight' =>  [
                'dropdown' => ['h3', 'p', 'blockquote'],
                'ico' => 'p'
            ],
        ],
        'btns' => [
            ['formattingLight'],
            ['bold', 'italic'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['undo', 'redo'],
            ['viewHTML'],
        ],
        'removeformatPasted' => true,
        'autogrow' => true,
    ],
    'plugins' => [],
]);
?>
```



For all configuration options see: https://alex-d.github.io/Trumbowyg/
